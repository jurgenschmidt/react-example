import React from 'react'
import data from '../data/dishes.json'

const AppContext = React.createContext()

class AppProvider extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      items: [],
      meal: '',
      numberOfPeople: 1,
      restaurant: '',
      data
    }
  }

  setMeal = item => {
    this.setState({ meal: item })
  }

  setNumberOfPeople = item => {
    this.setState({ numberOfPeople: item })
  }

  setRestaurant = item => {
    this.setState({ restaurant: item })
  }

  addItem = item => {
    let { items } = this.state

    const newItem = items.find(i => i.id === item.id)
    if (!newItem) {
      item.quantity = 1
      this.setState({
        items: this.state.items.concat(item)
      })
    } else {
      this.setState({
        items: this.state.items.map(item =>
          item.id === newItem.id
            ? Object.assign({}, item, { quantity: item.quantity + 1 })
            : item
        )
      })
    }
  }

  removeItem = item => {
    let { items } = this.state

    const newItem = items.find(i => i.id === item.id)
    if (newItem.quantity > 1) {
      this.setState({
        items: this.state.items.map(item =>
          item.id === newItem.id
            ? Object.assign({}, item, { quantity: item.quantity - 1 })
            : item
        )
      })
    } else {
      const items = [...this.state.items]
      const index = items.findIndex(i => i.id === newItem.id)

      items.splice(index, 1)
      this.setState({ items: items })
    }
  }

  render() {
    return (
      <AppContext.Provider
        value={{
          setMeal: this.setMeal,
          setNumberOfPeople: this.setNumberOfPeople,
          setRestaurant: this.setRestaurant,
          addItem: this.addItem,
          removeItem: this.removeItem,
          items: this.state.items,
          dishes: this.state.data.dishes,
          meal: this.state.meal,
          numberOfPeople: this.state.numberOfPeople,
          restaurant: this.state.restaurant
        }}
      >
        {this.props.children}
      </AppContext.Provider>
    )
  }
}

export function withContext(Component) {
  return function ContextComponent(props) {
    return (
      <AppContext.Consumer>
        {context => <Component {...props} context={context} />}
      </AppContext.Consumer>
    )
  }
}

export default AppProvider
