import React from 'react'
import { Link } from 'react-router-dom'
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'
import Button from 'react-bootstrap/Button'
import { withContext } from '../context/AppProvider'

class Dishes extends React.Component {
  state = {
    errors: {}
  }

  addItem(item) {
    const { id, name } = item
    this.props.context.addItem({ id, name })
  }

  removeItem(item) {
    this.props.context.removeItem(item)
  }

  handleClick(e) {
    const errors = this.validate(this.props.context)
    this.setState({ errors })
    if (Object.keys(errors).length >= 1) {
      e.preventDefault()
    }
  }

  validate(data) {
    const errors = {}
    const total = data.items.reduce((acc, item) => acc + item.quantity, 0)
    if (total > 10) errors.total = 'Maximum 10'
    if (total < data.numberOfPeople)
      errors.total = 'Number of dishes is less than people.'
    return errors
  }

  render() {
    const { errors } = this.state
    const { context } = this.props

    const dishes = context.dishes.filter(
      dish =>
        dish.restaurant === context.restaurant &&
        dish.availableMeals.includes(context.meal)
    )

    return (
      <div>
        <h1>Dishes</h1>
        {dishes.map(res => (
          <InputGroup key={res.id}>
            <FormControl disabled placeholder={res.name} />
            <InputGroup.Append>
              <Button
                variant="outline-secondary"
                onClick={this.addItem.bind(this, res)}
              >
                Add to cart
              </Button>
            </InputGroup.Append>
          </InputGroup>
        ))}
        <hr />
        {context.items &&
          context.items.map(item => (
            <InputGroup key={item.id}>
              <FormControl
                isInvalid={errors.total}
                disabled
                placeholder={item.name}
              />
              <InputGroup.Append>
                <Button
                  variant="success"
                  onClick={this.addItem.bind(this, item)}
                >
                  +
                </Button>
                <InputGroup.Text>{item.quantity}</InputGroup.Text>
                <Button
                  variant="danger"
                  onClick={this.removeItem.bind(this, item)}
                >
                  -
                </Button>
              </InputGroup.Append>
            </InputGroup>
          ))}
        {errors.total && (
          <div
            style={{ marginTop: `.25rem`, fontSize: `80%`, color: `#dc3545` }}
          >
            {errors.total}
          </div>
        )}
        <Link to="/restaurants">Prev</Link> -{' '}
        <Link to="/review" onClick={this.handleClick.bind(this)}>
          Next
        </Link>
      </div>
    )
  }
}

export default withContext(Dishes)
