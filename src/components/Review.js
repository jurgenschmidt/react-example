import React from 'react'
import { Link } from 'react-router-dom'
import CardGroup from 'react-bootstrap/CardGroup'
import Card from 'react-bootstrap/Card'
import ListGroup from 'react-bootstrap/ListGroup'
import { withContext } from '../context/AppProvider'

const Review = ({ context }) => {
  const handleClick = e => {
    e.preventDefault()
    const data = {
      meal: context.meal,
      numberOfPeople: context.numberOfPeople,
      restaurant: context.restaurant,
      dishes: context.items
    }
    console.log(data)
  }

  return (
    <div>
      <h1>Review and submit</h1>

      <CardGroup>
        <Card style={{ width: '18rem' }}>
          <Card.Header>Featured</Card.Header>
          <ListGroup variant="flush">
            <ListGroup.Item>Meal: {context.meal}</ListGroup.Item>
            <ListGroup.Item>
              Number of People: {context.numberOfPeople}
            </ListGroup.Item>
            <ListGroup.Item>Restaurant: {context.restaurant}</ListGroup.Item>
          </ListGroup>
        </Card>
        <Card style={{ width: '18rem' }}>
          <Card.Header>Dishes:</Card.Header>
          <ListGroup variant="flush">
            {context.items.map(item => (
              <ListGroup.Item key={item.id}>
                {item.name} - {item.quantity}
              </ListGroup.Item>
            ))}
          </ListGroup>
        </Card>
      </CardGroup>
      <Link to="/dishes">Prev</Link> -{' '}
      <a href="/" onClick={handleClick}>Submit</a>
    </div>
  )
}

export default withContext(Review)
