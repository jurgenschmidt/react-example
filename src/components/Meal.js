import React from 'react'
import { Link } from 'react-router-dom'
import Form from 'react-bootstrap/Form'
import { withContext } from '../context/AppProvider'

class Meal extends React.Component {
  state = {
    errors: {}
  }

  setMeal(event) {
    this.props.context.setMeal(event.target.value)
  }

  setNumberOfPeople(event) {
    this.props.context.setNumberOfPeople(event.target.value)
  }

  handleClick(e) {
    const errors = this.validate(this.props.context)
    this.setState({ errors })
    if (Object.keys(errors).length >= 1) {
      e.preventDefault()
    }
  }

  validate(data) {
    const errors = {}
    if (!data.meal) errors.meal = "Can't be blank"
    if (!data.numberOfPeople) errors.numberOfPeople = "Can't be blank"
    if (data.numberOfPeople > 10) errors.numberOfPeople = 'Maximum 10'
    return errors
  }

  render() {
    const { errors } = this.state
    const { context } = this.props

    return (
      <Form>
        <h1>Meal</h1>
        <Form.Group>
          <Form.Label>Please select a meal:</Form.Label>
          <Form.Control
            as="select"
            isInvalid={errors.meal}
            value={context.meal}
            onChange={this.setMeal.bind(this)}
          >
            <option value="">---</option>
            <option value="breakfast">Breakfast</option>
            <option value="lunch">Lunch</option>
            <option value="diner">Diner</option>
          </Form.Control>
          <Form.Control.Feedback type="invalid">
            {errors.meal}
          </Form.Control.Feedback>
        </Form.Group>

        <Form.Group>
          <Form.Label>Please enter the number of people: (max 10)</Form.Label>
          <Form.Control
            type="number"
            isInvalid={errors.numberOfPeople}
            value={context.numberOfPeople}
            onChange={this.setNumberOfPeople.bind(this)}
          />
          <Form.Control.Feedback type="invalid">
            {errors.numberOfPeople}
          </Form.Control.Feedback>
        </Form.Group>

        <Link to="/restaurants" onClick={this.handleClick.bind(this)}>
          Next
        </Link>
      </Form>
    )
  }
}

export default withContext(Meal)
