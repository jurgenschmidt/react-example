import React from 'react'
import { withRouter } from 'react-router-dom'
import Nav from 'react-bootstrap/Nav'

const routes = [
  { pathname: '/', name: 'Step 1' },
  { pathname: '/restaurants', name: 'Step 2' },
  { pathname: '/dishes', name: 'Step 3' },
  { pathname: '/review', name: 'Step 4' }
]

const NavComponent = ({ history, location }) => {
  const { pathname } = location
  const currentIndex = routes.findIndex(e => e.pathname === pathname)

  return (
    <Nav
      variant="pills"
      className="justify-content-center"
      defaultActiveKey={pathname}
      activeKey={pathname}
      onSelect={e => e.preventDefault}
    >
      {routes.map((route, index) => (
        <Nav.Item key={route.name}>
          <Nav.Link
            onSelect={e => history.push(route.pathname)}
            eventKey={route.pathname}
            disabled={index > currentIndex}
          >
            {route.name}
          </Nav.Link>
        </Nav.Item>
      ))}
    </Nav>
  )
}

export default withRouter(NavComponent)
