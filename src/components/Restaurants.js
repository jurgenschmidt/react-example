import React from 'react'
import { Link } from 'react-router-dom'
import Form from 'react-bootstrap/Form'
import { withContext } from '../context/AppProvider'

class Restaurants extends React.Component {
  state = {
    errors: {}
  }

  setRestaurant(event) {
    this.props.context.setRestaurant(event.target.value)
  }

  handleClick(e) {
    const errors = this.validate(this.props.context)
    this.setState({ errors })
    if (Object.keys(errors).length >= 1) {
      e.preventDefault()
    }
  }

  validate(data) {
    const errors = {}
    if (!data.restaurant) errors.restaurant = "Can't be blank"
    return errors
  }

  render() {
    const { errors } = this.state
    const { context } = this.props

    const restaurants = context.dishes
      .filter(dish => dish.availableMeals.includes(context.meal))
      .map(dish => dish.restaurant)
    let unique = [...new Set(restaurants)]

    return (
      <div>
        <h1>Restaurants</h1>
        <Form>
          <Form.Group>
            <Form.Label>Please select a restaurant:</Form.Label>
            <Form.Control
              as="select"
              isInvalid={errors.restaurant}
              value={context.restaurant}
              onChange={this.setRestaurant.bind(this)}
            >
              <option value="">---</option>
              {unique.map(res => (
                <option key={res}>{res}</option>
              ))}
            </Form.Control>
            <Form.Control.Feedback type="invalid">
              {errors.restaurant}
            </Form.Control.Feedback>
          </Form.Group>
        </Form>
        <Link to="/">Prev</Link> -{' '}
        <Link to="/dishes" onClick={this.handleClick.bind(this)}>
          Next
        </Link>
      </div>
    )
  }
}

export default withContext(Restaurants)
