import React, { Component } from 'react'
import { BrowserRouter, Route } from 'react-router-dom'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Nav from './components/Nav'
import Meal from './components/Meal'
import Restaurants from './components/Restaurants'
import Dishes from './components/Dishes'
import Review from './components/Review'
import AppProvider from './context/AppProvider'

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <AppProvider>
          <Container>
            <Nav />
            <hr />
            <Row className="justify-content-md-center">
              <Route exact path="/" component={Meal} />
              <Route exact path="/restaurants" component={Restaurants} />
              <Route exact path="/dishes" component={Dishes} />
              <Route exact path="/review" component={Review} />
            </Row>
          </Container>
        </AppProvider>
      </BrowserRouter>
    )
  }
}

export default App
